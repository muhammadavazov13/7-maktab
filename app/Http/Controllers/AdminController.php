<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\News;
use App\Rasm;
use App\Sinf;
use App\Kitob;
use App\Rasm_tur;
use App\Teacher;
use App\Student;
use App\Raxbariyat;
use Illuminate\Support\Facades\Session;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth',\App\Http\Middleware\AdminMiddleware::class]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
     

        $clear=(int)\request()->get('clear');
        $n=10;
        $sms=trim(\request()->get('sms'));
        if(empty($sms)){
            if($clear===1){
                Session::put('searc','');
            }
            else{
                $sms=Session::get('searc');
            }
        }
        else{
            Session::put('searc',$sms);
        }
        if(!empty($sms)){
            $new=News::orderBy('id','desc')->where('mss','LIKE',"%{$sms}%")->orwhere('titil','LIKE',"%{$sms}%")->paginate($n);
        }
        else{
            $new=News::orderBy('id','desc')->paginate($n);
        }
        $page=(int)\request()->get('page');
        if($page<=0){$page=1;}
        $d=$n*($page-1);
        return view('admin.home',
       ['news'=>$new,'d'=>$d,'sms'=>$sms,'page'=>$page,'active'=>1]);
    }
   
    public function News_save(Request $request){
        $validatedData = $request->validate([
            'titil' => 'required|min:10',
            'imgs' =>'mimes:jpeg,bmp,png,jpg',
            'mss' => 'required|min:20',
            'sana' => 'required',
        ]);
        $news=new News();
        $rasmName='';
        $T=$request->file('imgs');
        if(! empty ($T)){
            $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
            $request->file('imgs')->storeAs('/news/',$rasmName, 'public');
        }
        $news->titil=$_POST['titil'];
        $news->mss=$_POST['mss'];
        $news->img=$rasmName;
        $news->created_at=$_POST['sana'];
        $s=$news->save();
        return redirect('home?page='.$_POST['page']);
    }
    public function News_delete($id,Request $request)
    {

        $news=News::where('id',$id)->first();
        $img=$news->img;
       Storage::disk('public')->delete('/news/'.$img);
       $page=(int)\request()->get('page');

      
        $news=News::where('id',$id)->delete();
        return redirect('home?page='.$page);

    }
    public function News_edit($id,Request $request)
    {
        $news=News::where('id',$id)->first();
        $page=(int)\request()->get('page');
        return view('admin.News_edit',
            ['news'=>$news,'page'=>$page,'active'=>1]);

    }

    public function News_edit_save($id,Request $request)
    {
        $validatedData = $request->validate([
            'titil' => 'required|min:10',
            'sana'=>'required',
            'mss' => 'required|min:20',
        ]);
        if(!empty($request->file('imgs'))){
            $dd=News::where('id',$id)->first();
            $img=$dd->img;
            $validatedData = $request->validate([
               
                'imgs' =>'mimes:jpeg,bmp,png,jpg',
              
            ]);
            Storage::disk('public')->delete('/news/'.$img);
            $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
            $request->file('imgs')->storeAs('/news/',$rasmName, 'public');
        }
        else{
            $dd=News::where('id',$id)->first();
            $rasmName=$dd->img;
        }
        $titil=$_POST['titil'];
        $mss=$_POST['mss'];
        $sana=$_POST['sana'];
        $news=News::where('id',$id)->update(['titil' => $titil,'mss'=>$mss,'img'=>$rasmName,'created_at'=>$sana]);
        $page=(int)\request()->get('page');
        return redirect('home?page='.$page);
    }


    public function photo(Request $request)
    {
        $n=20;
        $sms=trim(\request()->get('sms'));
        if(!empty($tur)){
            $rasm=Rasm::orderBy('id','desc')->where('tur',$tur)->paginate($n);
        }
        else{
            $rasm=Rasm::orderBy('id','desc')->paginate($n);
        }
        $tur=Rasm_tur::All();
        $page=(int)\request()->get('page');
        if($page<=0){$page=1;}
        $d=$n*($page-1);
        return view('admin.photo',
       ['rasm'=>$rasm,'d'=>$d,'page'=>$page,'tur'=>$tur,'sms'=>$sms,'active'=>2]);
    }
    public function rasm_qosh()
    {
        $tur=Rasm_tur::All();
        return view('admin.rasm_add',
       ['tur'=>$tur,'active'=>2]);
    }
    public function rasm_save(Request $request)
    {
      
        $validatedData = $request->validate([
            'imgs' =>'mimes:jpeg,bmp,png,jpg',
            'tur' => 'required',
        ]);
        $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
        $request->file('imgs')->storeAs('/rasm/',$rasmName, 'public');
        $rasm=new Rasm();
        $rasm->tur=$_POST['tur'];
        $rasm->img=$rasmName;
       
        $rasm->save();

        return redirect('photo');
    }
    public function  rasm_delete($id,Request $request)
    {

        $news=Rasm::where('id',$id)->first();
        $img=$news->img;
       Storage::disk('public')->delete('/rasm/'.$img);
       $page=(int)\request()->get('page');

      
        $news=Rasm::where('id',$id)->delete();
        return redirect('photo?page='.$page);

    }
    public function teacher()
    {
        $tur=Teacher::All();
        $clear=(int)\request()->get('clear');
        $n=2;
        $sms=trim(\request()->get('sms'));
        if(empty($sms)){
            if($clear===1){
                Session::put('searchs','');
            }
            else{
                $sms=Session::get('searchs');
            }
        }
        else{
            Session::put('searchs',$sms);
        }
        if(!empty($sms)){
            $te=Teacher::where('name','LIKE',"%{$sms}%")->orwhere('fani','LIKE',"%{$sms}%")->orwhere('malumot','LIKE',"%{$sms}%")->paginate($n);
        }
        else{
            $te=Teacher::paginate($n);
        }
        $page=(int)\request()->get('page');
        if($page<=0){$page=1;}
        $d=$n*($page-1);
        return view('admin.teacher',
       ['teachers'=>$te,'d'=>$d,'sms'=>$sms,'page'=>$page,'active'=>3]);
    }
    public function teacher_save(Request $request)
    {
        $request->validate([
            'imgs' =>'mimes:jpeg,bmp,png,jpg|required',
            'name' => 'required|min:3',
            'malumot' => 'required|min:3',
            'age'=>'regex:/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
            'fan' => 'required|min:3',

        ]);
        $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
        $request->file('imgs')->storeAs('/teachers/',$rasmName, 'public');
        $rasm=new Teacher();
        $rasm->name=$_POST['name'];
        $rasm->img=$rasmName;
        $rasm->malumot=$_POST['malumot'];
        $rasm->fani=$_POST['fan'];
        $rasm->age=$_POST['age'];
        $rasm->save();
        $page=$_POST['page'];

        return redirect('teacher?page='.$page);
    }
    public function  teacher_delete($id,Request $request)
    {

        $news=Teacher::where('id',$id)->first();
        $img=$news->img;
        Storage::disk('public')->delete('/teachers/'.$img);
        $page=(int)\request()->get('page');

      
        $news=Teacher::where('id',$id)->delete();
        return redirect('teacher?page='.$page);

    }
    public function  teacher_edit($id,Request $request)
    {

        $te=Teacher::where('id',$id)->first();
       
       
        $page=(int)\request()->get('page');

      
        
        return view('admin.teacher_edit',
        ['teacher'=>$te,'page'=>$page,'active'=>3]);

    }
    public function  teacher_edit_save($id,Request $request)
    {
        $request->validate([
            
            'name' => 'required|min:3',
            'malumot' => 'required|min:3',
            'age'=>'regex:/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
            'fan' => 'required|min:3',

        ]);

        if(!empty($request->file('imgs'))){
            $validatedData = $request->validate([
               
                'imgs' =>'mimes:jpeg,bmp,png,jpg',
              
            ]);
            $dd=Teacher::where('id',$id)->first();
            $img=$dd->img;
            Storage::disk('public')->delete('/teachers/'.$img);
            $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
            $request->file('imgs')->storeAs('/student/',$rasmName, 'public');
        }
        else{
            $dd=Teacher::where('id',$id)->first();
            $rasmName=$dd->img;
        }
        $titil=$_POST['name'];
        $mss=$_POST['malumot'];
        $fani=$_POST['fan'];
        $age=$_POST['age'];
        $news=Teacher::where('id',$id)->update(['name' => $titil,'malumot'=>$mss,'img'=>$rasmName,'age'=>$age,'fani'=>$fani]);
      
        
        $page=$_POST['page'];

        return redirect('teacher?page='.$page);



    }
    public function student()
    {
        $tur=Teacher::All();
        $clear=(int)\request()->get('clear');
     
        $sms=trim(\request()->get('sms'));
        if(empty($sms)){
            if($clear===1){
                Session::put('search','');
            }
            else{
                $sms=Session::get('search');
            }
        }
        else{
            Session::put('search',$sms);
        }
        if(!empty($sms)){
            $te=Student::orderBy('id','desc')->where('name','LIKE',"%{$sms}%")->orwhere('malumot','LIKE',"%{$sms}%")->get();
        }
        else{
            $te=Student::orderBy('id','desc')->get();
        }
      
        return view('admin.student',
       ['students'=>$te,'d'=>0,'sms'=>$sms,'active'=>4]);
    }
    public function student_save(Request $request)
    {
        $request->validate([
            'imgs' =>'mimes:jpeg,bmp,png,jpg|required',
            'name' => 'required|min:3',
            'malumot' => 'required|min:5',
        ]);
        $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
        $request->file('imgs')->storeAs('/student/',$rasmName, 'public');
        $rasm=new Student();
        $rasm->name=$_POST['name'];
        $rasm->img=$rasmName;
        $rasm->malumot=$_POST['malumot'];
        $rasm->save();

        return redirect('student');
    }
    public function student_delete(Request $request,$id)
    {
        
       $news=Student::where('id',$id)->first();
       $img=$news->img;
      Storage::disk('public')->delete('/student/'.$img);

     
       $news=Student::where('id',$id)->delete();

        return redirect('student');
    }
    public function student_edit(Request $request,$id)
    {
        
       $te=Student::where('id',$id)->first();
      

        return view('admin.student_edit' ,['student'=>$te,'active'=>4]);
    }
    public function student_edit_save(Request $request,$id)
    {
        $request->validate([
            'name' => 'required|min:3',
            'malumot' => 'required|min:5',
        ]);
        if(!empty($request->file('imgs'))){
            $request->validate([
                'imgs' =>'mimes:jpeg,bmp,png,jpg|required',
            
            ]);
            $dd=Student::where('id',$id)->first();
            $img=$dd->img;
            Storage::disk('public')->delete('/student/'.$img);
            $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
            $request->file('imgs')->storeAs('/student/',$rasmName, 'public');
        }
        else{
            $dd=Student::where('id',$id)->first();
            $rasmName=$dd->img;
        }
        $titil=$_POST['name'];
        $mss=$_POST['malumot'];
        $news=Student::where('id',$id)->update(['name' => $titil,'malumot'=>$mss,'img'=>$rasmName]);
        
        return redirect('student');
    }
    public function raxbariyat()
    {
        
        $clear=(int)\request()->get('clear');
        
        $sms=trim(\request()->get('sms'));
        if(empty($sms)){
            if($clear===1){
                Session::put('sea','');
            }
            else{
                $sms=Session::get('sea');
            }
        }
        else{
            Session::put('sea',$sms);
        }
        if(!empty($sms)){
            $te=Raxbariyat::where('name','LIKE',"%{$sms}%")->orwhere('lavozim','LIKE',"%{$sms}%")->orwhere('email','LIKE',"%{$sms}%")->get();
        }
        else{
            $te=Raxbariyat::All();
        }
       
        return view('admin.raxbariyat',
       ['raxbariyat'=>$te,'d'=>0,'sms'=>$sms,'active'=>6]);
    }
    public function raxbariyat_save(Request $request)
    {
        $request->validate([
            'imgs' =>'mimes:jpeg,bmp,png,jpg|required',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'vaz' => 'required|min:20',
            'lavozim' => 'required|min:3',
            'tel' => 'regex:/^\+[0-9]{12}$/',
        ]);
        $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
        $request->file('imgs')->storeAs('/raxbariyat/',$rasmName, 'public');
        $rasm=new Raxbariyat();
        $rasm->name=$_POST['name'];
        $rasm->img=$rasmName;
        $rasm->tel=$_POST['tel'];
        $rasm->vazifasi=$_POST['vaz'];
        $rasm->email=$_POST['email'];
        $rasm->lavozim=$_POST['lavozim'];
        $rasm->save();


        return redirect('raxbariyat');
    }
    public function raxbariyat_delete(Request $request,$id)
    {
        
       $news=Raxbariyat::where('id',$id)->first();
       $img=$news->img;
      Storage::disk('public')->delete('/raxbariyat/'.$img);

     
       $news=Raxbariyat::where('id',$id)->delete();

        return redirect('raxbariyat');
    }
    public function raxbariyat_edit($id)
    {
        
       
       $te=Raxbariyat::where('id',$id)->first();
        return view('admin.raxbariyat_edit',
       ['raxbariyat'=>$te,'active'=>6]);
    }
    public function raxbariyat_edit_save(Request $request,$id)
    {
        $request->validate([
           
            'name' => 'required|min:3',
            'email' => 'required|email',
            'vaz' => 'required|min:20',
            'lavozim' => 'required|min:3',
            'tel' => 'regex:/^\+[0-9]{12}$/',
        ]);
       
       
      
        $name=$_POST['name'];
       
        $tel=$_POST['tel'];
        $vazifasi=$_POST['vaz'];
        $email=$_POST['email'];
        $lavozim=$_POST['lavozim'];
        if(!empty($request->file('imgs'))){
            $request->validate([
                'imgs' =>'mimes:jpeg,bmp,png,jpg|required',
                
            ]);
            $dd=Raxbariyat::where('id',$id)->first();
            $img=$dd->img;
            Storage::disk('public')->delete('/raxbariyat/'.$img);
            $rasmName='foto'.(string)\date('Y-m-d\TH-i-s').'.jpg';
            $request->file('imgs')->storeAs('/raxbariyat/',$rasmName, 'public');
        }
        else{
            $dd=Raxbariyat::where('id',$id)->first();
            $rasmName=$dd->img;
        }
      
        $news=Raxbariyat::where('id',$id)->update(['name' => $name,'tel'=>$tel,'vazifasi'=>$vazifasi,'lavozim'=>$lavozim,'img'=>$rasmName]);
        
        return redirect('raxbariyat');
    }
    public function library(Request $request)
    {
        $sinf = Sinf::All();
        $books= Kitob::All();
        $clear=(int)\request()->get('clear');
        
        $sms=trim(\request()->get('sms'));
        if(empty($sms)){
            if($clear===1 ){
                Session::put('se','');
            }
            else{
                $sms=Session::get('se');
            }
        }
        else{
            Session::put('se',$sms);
        }
        if(!empty($sms)){
            $books=Kitob::where('sinf',$sms)->get();
        }
        else{
            $books=Kitob::All();
        }
        if($sms=="0"){
            $books=Kitob::All();
        }
        return view('admin.library',
        ['sinf'=>$sinf,'books'=>$books,'sms'=>$sms,'active'=>7,'d'=>1]);
    }
    public function library_qoshish(Request $request)
    {
        $sinf = Sinf::All();
       
        return view('admin.library_add',
        ['sinf'=>$sinf,'active'=>7]);
    }
    public function library_save(Request $request)
    {
        $request->validate([
            'imgs' =>'mimes:doc,docs,pdf,pptx|required',
            'name' => 'required|min:3',
            'sinf' => 'required',
           
        ]);
        $nam = $request->file('imgs')->getClientOriginalName();
        $book = new Kitob();
        $rasmName=(string)\date('Y-m-d\TH-i-s').$nam;
        $request->file('imgs')->storeAs('/kitob/',$rasmName, 'public');
        $book->name=$_POST['name'];
        $book->sinf=$_POST['sinf'];
        $book->joyi=$rasmName;
        $book->save();
        return redirect('library');
    }
    public function library_edit(Request $request,$id)
    {
        $sinf = Sinf::All();
        $lin = Kitob::where('id',$id)->first();
        return view('admin.library_edit',['book'=>$lin,'sinf'=>$sinf,'active'=>7]);
    }
    public function library_edit_save(Request $request)
    {
        $request->validate([
            
            'name' => 'required|min:3',
            'sinf' => 'required',
           
        ]);
        $nam = $request->file('imgs')->getClientOriginalName();
        $book = new Kitob();
       
        if(!empty($request->file('imgs'))){
            $request->validate([
                'imgs' =>'mimes:jpeg,bmp,png,jpg|required',
                
            ]);
            $dd=Kitob::where('id',$id)->first();
            $img=$dd->img;
            Storage::disk('public')->delete('/raxbariyat/'.$img);
            $rasmName=(string)\date('Y-m-d\TH-i-s').$nam;
            $request->file('imgs')->storeAs('/kitob/',$rasmName, 'public');
        }
        else{
            $dd=Kitob::where('id',$id)->first();
            $rasmName=$dd->img;
        }
        $name=$_POST['name'];
        $sinf=$_POST['sinf'];
        $joyi=$rasmName;
        Kitob::where('id',$id)->update(['name'=>$name,'sinf'=>$sinf,'joyi'=>$rasmName]);
        return redirect('library');
    }
}
