@extends('layouts.user')

@section('content')

<section id="" class="team section-bg">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-12 col-md-8 ">
                <div class="section-title">
                    <h2>Maktab Kutubxonasi</h2>
                </div>
                @foreach($books as $book)
                <ul id="new" class="list-group list-group-flush" >
                    <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211);">
                        <p class="h1">
                            <i class="bx bxs-book"></i>  {{$book->name}} 
                            <a class="text-body " href="/download/{{$book->id}}"><i class="bx bxs-download"></i></a>
                        </p>
                    </li>
            
                </ul>
            @endforeach 
            <h1>{{$as}}</h1>
               
                
             
            </div>
            <div class="col-12 col-md-4 "  style="
            background-color: #f2f2f2;">
                <div class="section-title">
                    <h2>Kitob javoni</h2>
                </div>
                @foreach($sinf as $new)
                    <ul id="new" class="list-group list-group-flush" >
                        <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211);"><a class="text-body" href="/kutubxona?sinf={{$new->id}}">{{$new->name}}</a></li>
                
                    </ul>
                @endforeach 
              
            </div>
        </div>
    </div>
  </section>
@endsection