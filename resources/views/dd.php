<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Soni maktabi</title>
  <meta content="" name="description">    
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="" name="keywords">
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Vendor CSS Files -->
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="{{ asset('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i') }}" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/aos/aos.css" rel="stylesheet') }}">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v3.0.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="/">MAKTAB</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li ><a href="#about">Yangiliklar</a></li>
          <li  class="drop-down" ><a href="#portfolio">Maktab haqida</a>
            <ul>
              <li><a href="">Maktab Tarixi</a></li>
              <li><a href="">Raxbarlar</a></li>
              <li><a href="/">O'qituvchilar</a></li>
            </ul>
          </li>
          <li  class="drop-down">
            <a href="">O'quvchilarga</a>
            <ul>
              <li><a href="/jadval">Dars jadvali</a></li>
              <li><a href="/kutubxona">Kutubxona</a></li>
              <li><a href="/talabalar">Iqtidorli o'quvchilar</a></li>
            </ul>
          </li>
          <li><a href="/gallerya">Galleriya</a></li>
          <li><a href="/contact">Biz bilan bog'lanish</a></li>
        </ul>
      </nav>
      <a href="https://kundalik.com" class="get-started-btn scrollto">Kundalik.uz</a>
    </div>
  </header>
  <section id="hero" class="d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Maktabning to'liq nomi</h1>
          <h2>Maktab shiyori yoki yil nomi yozamizmi</h2>
          <div class="d-lg-flex">
            <a href="#about" class="btn-get-started scrollto">Yangiliklar</a>
            
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="rasmlar/school2.jpg" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Yangiliklar</h2>
        </div>
        <div class="row">
            @foreach($news as $new)
            <div class="card col-12 col-md-5 col-lg-3 mb-5" >
              <div class="portfolio-img"><img src="{{asset('/storage/news/'.$new->img)}}" class="img-fluid" alt=""></div>
              <div class="portfolio-info">
                <a href="{{asset('/storage/news/'.$new->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
                <p>{{$new->created_at}}</p>
              </div>
             
              <div class="card-body">
              <h5 class="card-title">{{$new->titil}}</h5>
              </div>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$new->id}}">
                Batafsil
              </button>
              <div class="modal fade" id="exampleModal{{$new->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$new->id}}" aria-hidden="true">
                <div class="modal-dialog  modal-dialog-scrollable modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel{{$new->id}}">{{$new->titil}}</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <p>{{$new->mss}}</p>
                    </div>
                    <div class="modal-footer">
                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            @endforeach 
            <div class="col-4 offset-4 justify-content-center mt-5">
              {{$news->links()}}
              
            </div>
          
        </div>
       

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= Why Us Section ======= -->

   
 <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Maktab haqida</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
          <li data-filter="*" class="filter-active">Hammasi</li>
          @foreach($tur as $tu)   
            <li data-filter=".{{$tu->id}}">{{$tu->name}}</li>
          @endforeach
        </ul>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
          @foreach($rasm as $ra)
          <div class="col-lg-4 col-md-6 portfolio-item {{$ra->tur}}">
            <div class="portfolio-img"><img src="{{ asset('storage/rasm')}}/{{$ra->img}}" class="img-fluid" alt=""></div>

            <div class="portfolio-info">
              <a href="{{ asset('storage/rasm')}}/{{$ra->img}}" data-gall="portfolioGallerys" class="venobox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>
          @endforeach
         
        </div>

      </div>
    </section>
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Iqtidorli o'quvchilar</h2>
        </div>
        <div class="row">
          @foreach($students as $st)
          <div class="col-xl-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
            <div class="icon"><img src="{{ asset('/storage/student/'.$st->img)}}" class="img-fluid" alt=""></div>
              <h4><a > {{$st->name}}</a></h4>
              <p>{{$st->malumot}}</p>
            </div>
          </div>
          @endforeach
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
   

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Maktabimiz Rahbariyati</h2><br>
          
         
        </div>

<div class="text-center">
  <button type="button" class="btn btn-primary mb-5"  data-toggle="modal" data-target="#exampleModal">
    Fan o'qituvchilari
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-scrollable modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">O'qituvchilar </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="d-flex mb-4 mt-1">
            <div class="text-center" style="width: 5%">Id</div>
            <div class="text-center" style="width: 20%">FIO</div>
            <div class="text-center" style="width: 15%">Tug'ulgan sana</div>
            <div class="text-center" style="width: 15%">Mallumoti</div>
            <div class="text-center" style="width: 20%">Mutaxasisligi</div>
            <div class="text-center" style="width: 25%">Rasm</div>
           
        </div>
          @foreach($teacher as $te)  
          <div class="d-flex mb-4 mt-1">
              <div class="text-center" style="width: 5%">{{++$d}}</div>
              <div class="text-center" style="width: 20%">
              <p>{{$te->name}}</p>
              </div>
             
              <div class="text-center" style="width: 15%">
                  <p>{{$te->age}}</p>
              </div>
              <div class="text-center" style="width: 15%">
                  <p>{{$te->malumot}}</p>
              </div>
              
              <div class="text-center" style="width: 20%">
                  <p>{{$te->fani}}</p>
              </div>
              <div class="text-center" style="width: 25%">
                  <div class="portfolio-img"><img src="{{asset('/storage/teachers/'.$te->img)}}" class="img-fluid" style="max-height: 15vh" alt=""></div>
                  <div class="portfolio-info">
                      <a href="{{asset('/storage/teachers/'.$te->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
                  </div>
              </div>
             
          </div>         
          @endforeach

        </div>
        <div class="modal-footer">
       
        </div>
      </div>
    </div>
  </div>

</div>
       



        <div class="row">
          @foreach ($raxbariyat as $raxbar)
              
          
          <div class="col-lg-6">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
              <div style="overflow: hidden;
              width: 180px;
              "><img src="{{ asset('storage/raxbariyat')}}/{{$raxbar->img}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>{{$raxbar->name}}</h4>
                <span></span>
                <p>{{$raxbar->malumot}}</p>
               
              </div>
            </div>
          </div>
          @endforeach
            
        </div>

      </div>
    </section><!-- End Team Section -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Biz bilan bog'lanish</h2>
          <p>Sizning fikringiz biz uchun muhum</p>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Manzil:</h4>
                <p>Jizzax viloyati G'allaorol tumani 54-Maktab </p>
              </div>

              <div class="email">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <p>info@example.com</p>
              </div>

              <div class="phone">
                <i class="icofont-phone"></i>
                <h4>Telefon:</h4>
                <p>+998995848650</p>
              </div>

            </div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="/send" method="post" style=" border-top: 3px solid #47b2e4; border-bottom: 3px solid #47b2e4;">
              @csrf
              <div class="form-group row mt-3">
                <div class="form-group col-md-6">
                  <label for="name">FIO</label>
                  <input type="text" name="name" class="form-control" id="name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Sizning Email</label>
                  <input type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Murojat turi</label>
                <input type="text" class="form-control" name="subject" id="subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <label for="name">Xabar</label>
                <textarea class="form-control" name="message" rows="10" data-rule="required" data-msg="Please write something for us"></textarea>
                <div class="validate"></div>
              </div>
             
              <nav class="text-center mb-3"><button class="btn btn-primary" type="submit">Jo'natish</button></nav>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->

  <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js')}}"></script>
  <script src="{{ asset('/js/jquery.js')}}"></script>
<script>

</script>
</body>

</html>