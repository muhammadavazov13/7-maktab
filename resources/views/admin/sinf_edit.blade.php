@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 ">
            <a href="/sinflar" class="btn btn-outline-primary m-4">  
                 <span id="qayt" class="fas fa-chevron-left "></span>
            </a>
        </div>
        <div class="col-12 ">
            <h1 class="text-center">Sinf tahrirlash </h1>
        </div>
        <div class="col-9 mb-5">
            <form action="/sinflar/edit/save?id={{$edit->id}}" method="post" enctype="multipart/form-data">
                <div class="form-group mb-5 mt-5">
                    <label  for="Lavozim" >Sinf nomi</label>
                    <input type="text" class="form-control" id="Lavozim" name="name" placeholder="Sinf nomi" value="{{$edit->name}}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror    
                <button type="submit" class="btn btn-primary ">Qo'shish</button>
                {{csrf_field()}}
            </form>
        </div>
    </div>
</div>
@endsection