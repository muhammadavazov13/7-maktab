


@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
               
            </div>



<div class="row">
    <div class="card mb-4">
        <div class="card-header" >
                <i class="fas fa-table me-1"></i>
               Kutubxona
               <a style="float: right;" class=" btn btn btn-outline-success " href="/library/qoshish"><i class="bi bi-plus-circle"></i></a>
        </div>

        <div class="card-body">

<table id="datatablesSimple" class="table-dark table">

<thead>
    <tr>
        <th>#</th>
        <th>Nomi</th>
        <th>Turi</th>
        <th>Sana</th>
        <th>Action</th>
    </tr>
</thead>
<tfoot>
    <tr>
    <th>#</th>
        <th>Nomi</th>
        <th>Turi</th>
        <th>Sana</th>
        <th>Action</th>
    </tr>
</tfoot>
<tbody>
@foreach($books as $b)  
    <tr>
        <td>{{++$d}}</td>
        <td>{{$b->name}}</td>
        <td> @foreach($sinf as $sin)  
                @if($sin->id==$b->sinf)
                    {{$sin->name}}
                @endif
            @endforeach</td>
        <td>{{$b->created_at}}</td>


        <td> <a href="/library/edit/{{$b->id}}" class="btn btn-outline-primary"><i
                    class="text-500 fas fa-edit"></i></a> <a
                href="/library/delete/{{$b->id}}" class="btn btn-outline-primary"><i
                    class="text-500 fas fa-trash-alt"></i></a>
</td>
    </tr>
    @endforeach
</tbody>
</table>

</div>
</div>
</div>
@endsection
