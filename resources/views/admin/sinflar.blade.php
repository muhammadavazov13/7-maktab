@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
               
            </div>



<div class="row">
    <div class="card mb-4">
        <div class="card-header" >
                <i class="fas fa-table me-1"></i>
               NEWS
               <a style="float: right;" class=" btn btn btn-outline-success " href="/sinflar/add "><i class="bi bi-plus-circle"></i></a>
        </div>

        <div class="card-body">

<table id="datatablesSimple" class="table-dark table">

<thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Sana</th>
        <th>Action</th>
    </tr>
</thead>
<tfoot>
    <tr>
    <th>#</th>
        <th>Name</th>
        <th>Sana</th>
        <th>Action</th>
    </tr>
</tfoot>
<tbody>
    @foreach($sinflar as $s)
    <tr>
        <td>{{++$d}}</td>
        <td>{{$s->name}}</td>
        <td>{{$s->created_at}}</td>
      

        <td> <a href="/sinflar/edit/?id={{$s->id}}" class="btn btn-outline-primary"><i
                    class="text-500 fas fa-edit"></i></a> <a
                href="/sinflar/delete/?id={{$s->id}}" class="btn btn-outline-primary"><i
                    class="text-500 fas fa-trash-alt"></i></a>

</td>
    </tr>
    @endforeach
</tbody>
</table>

</div>
</div>
</div>
@endsection






