@extends('layouts.user')

@section('content')





  <div id="team" class="team section-bg">
        <div class="container" data-aos="fade-up">
  
        <div class="section-title">
            <h2>Rahbarlar</h2>
          </div>
  
          <div class="row">
          @foreach ($raxbariyat as $raxbar)
       
            <div class="col-lg-6 mt-4 mt-lg-0" style="margin-bottom:20px;">
            <a href="/manager/{{$raxbar->id}}">
              <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100" >
                <img src="{{ asset('storage/raxbariyat')}}/{{$raxbar->img}}" class="imgfl" alt="">

                <div class="member-info">
                  <h4>{{$raxbar->name}}</h4>
                  <span>Jizzax shahri 7-maktab {{$raxbar->lavozim}}</span>
                  <hr style="border: 3px splid blue">
                      <h4> <i class="icofont-envelope"></i>Email:   {{$raxbar->email}}</h4>
                      <h4> <i class="icofont-phone"></i>Tell:   {{$raxbar->tel}}</h4>      
                </div>
              </div>
            </a>
            </div>
         
            @endforeach 
  
              
            </div>
  
          </div>
  
        </div>
    </div> 
  <style>
    .imgfl{
        height: 170px;
        width:auto;
        
    }
  </style>
@endsection