@extends('layouts.user')

@section('content')
<div class=" "   style="background-color: #f2f2f2; float: right; width:30%;">
    <div class="section-title">
        <h2>Raxbarlaar</h2>
    </div>
    @foreach($raxbars as $new)
        <ul id="new" class="list-group list-group-flush" >
            <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211);"><a class="text-body" href="/manager/{{$new->id}}">  {{$new->name}}</a></li>
    
        </ul>
    @endforeach 
</div>





<div id="portfolio-details" class="portfolio-details" style="margin-bottom: 100px; width:70%; ">
                <div class="container"  >
                    <div class="row gy-4">
                        <div class="col-lg-8 card-body"  > >
                            <div class="portfolio-details-slider swiper-container">
                                <div class="swiper-wrapper align-items-center">
                                        <div class="swiper-slide" >                                 
                                            <img src="{{ asset('storage/raxbariyat')}}/{{$raxbar->img}}" alt="" class="card-img-top" height="500px">
                                        </div>
                                <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 card-info ">

                            <h3>{{$raxbar->name}}</h3>
                            <ul>
                                <li><strong>Email</strong>:{{$raxbar->email}}</li>
                                <li><strong>Telefon</strong>:{{$raxbar->tel}}</li>
                                <li><strong>Lavozimi</strong>: {{$raxbar->lavozim}}</li>
                            </ul>

                            <div class="portfolio-description card-description">
                                <h2>Maktab {{$raxbar->lavozim}}i vazifalari</h2>
                                <p>
                                    {{$raxbar->vazifasi}}
                                </p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
@endsection