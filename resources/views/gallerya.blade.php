@extends('layouts.user')

@section('content')



<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

<!-- GAlary css -->

    <link href="css/glightbox.min.css" rel="stylesheet">   
    <link href="css/galery.css" rel="stylesheet">  




<div id="" class="portfolio">
    <div class="" data-aos="fade-up">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 ">
                <div class="section-title">
                    <h2>Maktab Gallerya</h2>
                </div>
           
                 <!-- <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                    @foreach($rasm as $ra)
                        <div class="col-lg-4 col-md-6 col-12 portfolio-item {{$ra->tur}}">
                            <div class="portfolio-img"><img src="{{ asset('storage/rasm')}}/{{$ra->img}}" class="w-100" alt=""></div>

                            <div class="portfolio-info">
                                <a href="{{ asset('storage/rasm')}}/{{$ra->img}}" data-gall="portfolioGallerys" class="venobox preview-link"><i class="bx bx-plus"></i></a>
                            </div>
                        </div>
                    @endforeach
                
                </div> -->
            </div>
           
        </div>
    </div>
</div> 



<div class="portfolio" id="portfolio">
    <div class="container"> 

        <div class="row">
           <div class="col-lg-12 d-flex justify-content-center"> 
           <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
                    <li data-filter="*" class="filter-active" style="width: 25%;">Hammasi</li>
                    @foreach($tur as $tu)   
                    <li data-filter=".{{$tu->id}}" style="width: 25%;">{{$tu->name}}</li>
                    @endforeach
                </ul> 
          </div>
        </div> 

      <div class="row portfolio-container">
      @foreach($rasm as $ra)
        <div class="col-lg-4 col-md-6 portfolio-item {{$ra->tur}}">
          <div class="portfolio-wrap">
            <img src="{{ asset('storage/rasm')}}/{{$ra->img}}" class="img-fluid" alt="">
            <div class="portfolio-info galery-bg">
              <!-- <h4>{{$tu->name}}</h4> -->
              <p><i class="icofont-ui-calendar"></i>  {{$ra->created_at}}</p><br>
              <div class="portfolio-links">
                <a href="{{ asset('storage/rasm')}}/{{$ra->img}}" data-gallery="portfolioGallery" class="portfolio-lightbox"   title="<i class=icofont-ui-calendar></i> {{$ra->created_at}}<br>{{$ra->tur}}"><i class="bi bi-plus-lg"></i></a>
              </div>
            </div>
          </div>
        </div>
      @endforeach

      </div>
    </div>
      
   

  
</div>
  





  <!-- Template Main JS File -->
  <script src="js/glightbox.min.js"></script>
  <script src="js/swiper-bundle.min.js"></script>
 <script src="js/galary.js"></script> 
@endsection